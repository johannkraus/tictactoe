;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Game   : TIC TAC TOE
;; Authors: Johann M. Kraus
;;          Markus Maucher
;; Date   : 21.07.2010
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; board layout
;;
;; a3 | b3 | c3
;; ------------
;; a2 | b2 | c2
;; ------------
;; a1 | b1 | c1

(def board (ref {:a3 "." :b3 "." :c3 "." :a2 "." :b2 "." :c2 "." :a1 "." :b1 "." :c1 "."}))

(defn reset-board []
  (dosync (ref-set board {:a3 "." :b3 "." :c3 "." :a2 "." :b2 "." :c2 "." :a1 "." :b1 "." :c1 "."})))

(defn print-layout []
  (println "\nChoose one of the following fields to place your mark (e.g. a1):\n\n"
	   "a3" "|" "b3" "|" "c3" "\n ------------\n"
	   "a2" "|" "b2" "|" "c2" "\n ------------\n"
	   "a1" "|" "b1" "|" "c1" "\n"))

(defn print-board [b]
  (println "\n" (:a3 b) "|" (:b3 b) "|" (:c3 b) "\n ---------\n"
	        (:a2 b) "|" (:b2 b) "|" (:c2 b) "\n ---------\n"
	        (:a1 b) "|" (:b1 b) "|" (:c1 b) "\n"))

(defn valid-turn? [position]
  (= (position @board) "."))

(defn turn [position player]
  (if-not (valid-turn? position)
    (println "This is not a valid turn!")
    (dosync (alter board assoc position player))))

(defn won? [board]
  (let [a1 (:a1 board)
	a2 (:a2 board)
	a3 (:a3 board)
	b1 (:b1 board)
	b2 (:b2 board)
	b3 (:b3 board)
	c1 (:c1 board)
	c2 (:c2 board)
	c3 (:c3 board)]
    (or
     (and (not= "." a1) (= a1 a2 a3))
     (and (not= "." b1) (= b1 b2 b3))
     (and (not= "." c1) (= c1 c2 c3))
     (and (not= "." a1) (= a1 b1 c1))
     (and (not= "." a2) (= a2 b2 c2))
     (and (not= "." a3) (= a3 b3 c3))
     (and (not= "." a1) (= a1 b2 c3))
     (and (not= "." a3) (= a3 b2 c1)))))

(defn game-end? [board]
  (or
   (every? #(not= "." (second %)) board)
   (won? board)))

(defn parse-input []
  (let [in (read-line)
	pos (keyword in)]
    (cond (= in "h") (do (println "Press h for help.\nPress b for current board.\nPress e to exit game.") (print-layout) (recur))
	  (= in "b") (do (print-board @board) (recur))
	  (= in "e") (System/exit 0)
	  (not (valid-turn? pos)) (do (println "This is not a valid turn. Press h for help.") (recur))
	  (valid-turn? pos) pos)))

(defn move-human []
  (parse-input))

(defn whichopt
  "Return action with minimal output"
  ([func values]
     (if (empty? values) nil (whichopt func (rest values) (first values) 0 1)))
  ([func values minvalue minindex actindex]
     (cond (empty? values) minindex
	   (func (first values) minvalue) (recur func (rest values) (first values) actindex (inc actindex) )
	   :else (recur func (rest values) minvalue minindex (inc actindex)))))

(defn minimax
  "Maximize score for player x, minimize for player o"
  [board player]
  (let [positions (filter #(= "." (board %)) (keys board))
	nextboards (map #(assoc board % player) positions)]
    ;;(print-board board)
    (if (= player "x")
      (cond (won? board) -1
	    (game-end? board) 0
	    :else (apply max (map #(minimax % "o") nextboards)))
      ;; else: Player is "o"
      (cond (won? board) 1
	    (game-end? board) 0
	    :else (apply min (map #(minimax % "x") nextboards))))))

(defn opt-move
  ""
  [board player]
  (let [positions (filter #(= "." (board %)) (keys board))
	nextboards (map #(assoc board % player) positions)
	nextplayer (if (= player "x") "o" "x")
	nexteval (map #(minimax % nextplayer) nextboards)
	nextcomp (if (= player "x") > <)]
    (nth positions (whichopt nextcomp nexteval))))

(defn move-ai []
  (opt-move @board "o"))

(defn tictactoe []
  (reset-board)
  (println "\nTIC TAC TOE")
  (let [cur-player (ref "o")]
    (while (not (game-end? @board))
	   (print-board @board)
	   (dosync (ref-set cur-player (if (= @cur-player "x") "o" "x")))
	   (println "Player" @cur-player "move (h for help):")
	   (let [pos (if (= @cur-player "x") (move-ai) (move-ai))]
	     (turn pos @cur-player)))
    (print-board @board)
    (if (won? @board)
      (println "Player" @cur-player "wins.")
      (println "The game ends in a tie.")))
  (Thread/sleep 2000)
  (recur))

(tictactoe)

